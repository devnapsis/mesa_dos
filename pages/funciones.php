<?php
function UsuariosConectados(){

	$params = array();

	require_once 'conexiones.php';
	$dia=date("Y-m-d");
	$arrData = select_usuarios_conectados($dia);

	$arrValues1 = implode(",", $arrData["RES1"]);
	$arrValues2 = implode(",", $arrData["RES2"]);
	$params["ARRVALUES1"] = $arrValues1;
	$params["ARRVALUES2"] = $arrValues2;
	
	$arrData = $arrData["RES1"];

	$maxsnd = 0;
	$horasnd = '';
	$maxmateo= 0;
	$horamateo = '';
	$snd = 0;
	$mateo = 0;
	$apo = 0;
	$maxapo= 0;
	$horaapo = '';

	$maxarr = (count($arrData)) - 1 ;

	unset($arrData[$maxarr]);
	foreach ($arrData as $key => $data)
	{
		$arr=explode(",",preg_replace('/\[|]|\'/', '', $data));

		// 		print_r($arr);

		$sumasnd = $arr[1];
		if($sumasnd >= $maxsnd)
		{
			$maxsnd = $sumasnd;
			$horasnd = $arr[0];
			$snd = $arr[1];
		}

		$sumamateo = $arr[2];
		if($sumamateo >= $maxmateo)
		{
			$maxmateo = $sumamateo;
			$horamateo = $arr[0];
			$snd = $arr[2];
		}

		$sumaapo = $arr[3];
		if($sumaapo >= $maxapo)
		{
			$maxapo = $sumaapo;
			$horaapo = $arr[0];
			$snd = $arr[3];
		}

	}

	if(empty($arrValues)){
		$errMsj= "<strong>No se reciben Datos</strong>";
		$err=true;
	}

	//$arrUltimo=explode(",",preg_replace('/\[|]|\'/', '', $arrData[count($arrData)-1]));
	$arrUltimo=explode(",",preg_replace('/\[|]|\'/', '', $arrData[count($arrData)-2]));

	$params["LAST_SND"] = $arrUltimo[1];
	$params["MAX_SND"] = $maxsnd;
	$params["TIME_MAX_SND"] = $horasnd;

	$params["LAST_MATEO"] = $arrUltimo[2];
	$params["MAX_MATEO"] = $maxmateo;
	$params["TIME_MAX_MATEO"] = $horamateo;

// 	echo "<pre>";
// 	print_r($params);
// 	echo "</pre>";


	return $params;
}
function getIssuesDevueltosOperaciones($opcionMesa){
	require_once 'conexiones.php';
	$datosIssuesOps = issuesOps($opcionMesa);
	return $datosIssuesOps;
}

function getTotalIssuesDevueltosOps($opcionMesa){
        require_once 'conexiones.php';
        $TotalIssuesOps = TotalIssuesDevueltosOps($opcionMesa);
        return $TotalIssuesOps;
}

function getIssuesSAC($opcionMesa){
	require_once 'conexiones.php';
	$datosIssuesSAC = issuesSac($opcionMesa);
	return $datosIssuesSAC;
}

function getIssuesCliente($opcionMesa){
        require_once 'conexiones.php';
        $datosIssuesCliente = issuesCliente($opcionMesa);
        return $datosIssuesCliente;
}

function getLlamadas(){
	require_once 'conexiones.php';
	$seg = 30;
	$params = array();
	$dia=date("Y-m-d");
	$params = asterisk($dia,$seg);
	return $params;
}

function getIssuesErrorProyecto($opcionMesa){
	require_once 'conexiones.php';
	$datosErrorProyecto = issuesErrorProyecto($opcionMesa);
	return $datosErrorProyecto;
}

function getIssuesHaciendo($opcionMesa){
	require_once 'conexiones.php';
	$issuesHaciendo = issuesHaciendo($opcionMesa);
	return $issuesHaciendo;
}

function getPlazoIssues(){
	require_once 'conexiones.php';
	$plazoIssues = PlazoIssues();
	return ($plazoIssues);
}


function getIssuesTopSecciones($opcionMesa){
	require_once 'conexiones.php';
	$plazoIssues = IssuesTopSecciones();
	return ($plazoIssues);
}

function getIssuesCerrados($opcionMesa){
        require_once 'conexiones.php';
        $cerradosDia = IssuesCerradosDia($opcionMesa);
	$cerradosSemana = IssuesCerradosSemana($opcionMesa);
	$cerradosMes = IssuesCerradosMes($opcionMesa);
	$arrData['Dia']=$cerradosDia['cantidadCerradosDia'];
	$arrData['Semana']=$cerradosSemana['cantidadCerradosSemana'];
	$arrData['Mes']=$cerradosMes['cantidadCerradosMes'];
        return ($arrData);
}

function getCerradosCreados($opcionMesa){

	require_once 'conexiones.php';
	$IssuesCerradosCreados = CerradosCreados($opcionMesa);
	return ($IssuesCerradosCreados);
}

function getIssuesCerradosHechos($opcionMesa){

	require_once 'conexiones.php';
	$IssuesCerradosHechos = issuesCerrados($opcionMesa);
	return ($IssuesCerradosHechos);
}

function getIssuesCerradosAno($opcionMesa){

	require_once 'conexiones.php';
	$IssuesCerradosAno = issuesCerradosAno($opcionMesa);
	return ($IssuesCerradosAno);
}

function getIssuesCerradosDia($opcionMesa){

	require_once 'conexiones.php';
	$IssuesCerradosDia = issuesCerradosDia($opcionMesa);
	return ($IssuesCerradosDia);
}

function getIssuesCreaURL($opcionMesa){

        require_once 'conexiones.php';
        $IssuesCreaURL = CreaURL($opcionMesa);
        return ($IssuesCreaURL);
}

function getIssuesCreadosVSAnoAnterior($opcionMesa){

        require_once 'conexiones.php';
        $IssuesCreadosAnoAnterior = CreadosAnoAnterior($opcionMesa);
        return ($IssuesCreadosAnoAnterior);
}
