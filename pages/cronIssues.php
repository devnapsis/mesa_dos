<?php
//echo "1";
//exit;
include_once 'funciones.php';
require_once ('phpmailer/class.phpmailer.php');

$plazoIssues = getPlazoIssues();

//echo "</pre>";
//echo "<pre>";
//print_r($plazoIssues["PlazoIssues"]);
//exit;
if(is_array($plazoIssues["PlazoIssues"]))
{
	foreach ($plazoIssues["PlazoIssues"] as $key => $datos) {
		if($datos["dias_restantes"]<=5)
		{
			//echo "Envía mail";
			//echo $datos["Nombre_Proyecto"];
			//echo $datos["ID"];
			//echo $datos["ESTADO"];
			//echo $datos["CATEGORIA"];
			//echo $datos["Fecha_Comprometida"];
			//echo $datos["ASIGNADO_A"];
			//echo $datos["email_operaciones"];
			//echo $datos["CREADO_POR"];
			//echo $datos["email_mesa"];
			//echo $datos["RESUMEN"];
			//echo $datos["CONTENIDO_ISSUE"];


			$AgenteMesa = explode(' ', $datos["CREADO_POR"]);
			$IngOperaciones = explode(' ', $datos["ASIGNADO_A"]);
			$ingAsociado = ($IngOperaciones[0]!='') ? ', '.$IngOperaciones[0] : '';

			$mail = new PHPMailer();

	        $mail->IsSMTP();
	        $mail->Host = 'mes.napsis.cl';
	        $mail->SMTPAuth   = true;
	        $mail->Port       = 25;
	        $mail->Username   = "mensajeria@mes.napsis.cl";
	        $mail->Password   = "msjnps2015";
	        $mail->IsHTML(true);
	        $mail->From = "operaciones@napsis.cl";
	        $mail->FromName = "Operaciones Mateonet";
	        $mail->SMTPDebug  = 1;
	        $dia_s = ($datos["dias_restantes"] == 1 ? '' : 's');
	        if($datos["dias_restantes"] < 0)
	        {
	        	$titulo = "[IMPORTANTE] Tienes issues con plazo de entrega vencido";
	        	$mensajePlazo = "con <b>".str_replace('-', '', $datos["dias_restantes"]). ' d&iacute;a'.$dia_s.' de atraso.</b> <br>';
	        }
	        else
	        {
	        	if ($datos["dias_restantes"] == 0)
	        	{
	        		$titulo = "[IMPORTANTE] Tienes issues con plazo de entrega para hoy";
		        	$mensajePlazo = ' que <b>vence hoy</b><br>';
	        	}
	        	else
	        	{
		        	$titulo = "[IMPORTANTE] Tienes issues con plazo de entrega pronto a vencer";
		        	$mensajePlazo = ' que <b>vence en '. $datos["dias_restantes"]. ' d&iacute;a'.$dia_s.' m&aacute;s. </b><br>';
	        	}
	        }      
	        $n = ($ingAsociado) ? 'n' : '';
	        $s = ($ingAsociado) ? 's' : '';
	        $cuerpo_mensaje = $AgenteMesa[0] .''. $ingAsociado.': Le'.$s.' recuerdo que tiene'.$n.' un issue pendiente de entrega a cliente '.$mensajePlazo.
						        'Los datos del issue son los siguientes:<p>

						        	<table rules="all" style="border-color: #666;" cellpadding="10">
		        					<tr style="background: #eee;"><td colspan="2"><strong><h2>Issue N&deg; '.$datos["ID"].'</h2></td><tr>
		        					<tr><td><strong>Proyecto: </td><td>'.$datos["Nombre_Proyecto"].'</td><tr>
		        					<tr style="background: #eee;"><td>Estado: </td><td>'.$datos["ESTADO"].'</td><tr>
		        					<tr><td><strong>Categoria: </td><td>'.$datos["CATEGORIA"].'</td><tr>
		        					<tr style="background: #eee;"><td>Estado: </td><td>'.$datos["ESTADO"].'</td><tr>
		        					<tr><td><strong>Resumen: </td><td>'.$datos["RESUMEN"].'</td><tr>
		        					<tr style="background: #eee;"><td>Contenido: </td><td>'.$datos["CONTENIDO_ISSUE"].'</td><tr>
		        					<tr><td><strong>Fecha comprometida seg&uacute;n issue: </td><td>'.$datos["Fecha_Comprometida"].'</td><tr></table><p>

		        				**Esto correo se genera autom&aacute;ticamente y corresponde a un servicio que consulta la fecha estimada de entrega de aquellas solicitudes que no han sido cerradas o que su estado es diferente a SAC Cliente.<p>
		        				Esperando tenga'.$n.' un buen d&iacute;a.<br>
		        				Su servidor Eventum';

	        $mail->Subject = $titulo;
	        $JefeSac = '';
	        $JefeOps = '';

	        if($datos["email_mesa"] != 'vcastillo@napsis.cl')
	        {
	        	$JefeSac = 'vcastillo@napsis.com';
	        	$mail->addCC($JefeSac,'Viviana');
	        }
	        else
	        {
	        	$datos["email_mesa"] = 'vcastillo@napsis.com';
	        }

	        if($datos["email_operaciones"] != 'aaguilar@mateonet.cl')
	        {
	        	$JefeOps = 'aaguilar@mateonet.cl';
	        	$mail->addCC($JefeOps,'Alvaro');
	        }

	        $mail->AddAddress($datos["email_mesa"],$AgenteMesa[0]);
	        ($ingAsociado) ? $mail->AddAddress($datos["email_operaciones"],$IngOperaciones[0]):'';
	        //Test Correo
	        //$mail->addCC('aaguilar@mateonet.cl','Alvaro');
	        
	        $html = "<html><head></head><body>";
	        $body  = $cuerpo_mensaje;
	        $mail->Body = $html. $body;
	        //$mail->Body .= "<br> <br> \r\n \r\n---\r\n Servidor Eventum";
	
	        $mail->Body .= ' </body></html>';
	        //echo "------------------------------------------------------<p>";
	        //print_r($mail);
        	$resp = $mail->Send();        		
		}
	}
}