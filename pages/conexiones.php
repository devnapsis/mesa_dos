<?php

function select_usuarios_conectados($hoy){

	$link =  mysqli_connect('192.168.5.27', 'root', 'tec.wor_08');
	mysqli_select_db($link,'Sineduc_usuarios_conectados');

	$query ="(
	Select
	date_format(from_unixtime(uc.uco_time), '%H:%i') as HORA,
	uc.uco_cantidad_usuario CANT,
	1 as SYSTEM
	FROM
	usuarios_conectados uc
	WHERE
	date_format(from_unixtime(uc.uco_time),'%Y-%m-%d') = '$hoy'
	) UNION (
	Select
	date_format(from_unixtime(mc.uco_time), '%H:%i') as HORA,
	mc.uco_cantidad_mateo CANT,
	9 as SYSTEM
	FROM
	mateo_conectados mc
	WHERE
	date_format(from_unixtime(mc.uco_time),'%Y-%m-%d') = '$hoy'
	)
	UNION (
	Select
	date_format(from_unixtime(ac.uco_time), '%H:%i') as HORA,
	ac.uco_cantidad_apoderado CANT,
	2 as SYSTEM
	FROM
	apoderados_conectados ac
	WHERE
	date_format(from_unixtime(ac.uco_time),'%Y-%m-%d') = '$hoy'
	)
	;

	";
	// 	print_r($query);
	#date_format(from_unixtime(uc.uco_time),'%Y-%m-%d') = '$hoy'

	$arrUser = array();
	$res = mysqli_query($link, $query);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$arrUser[$row["HORA"]][$row["SYSTEM"]] = $row["CANT"];
	}

	$arrSystem[1] = "SND";
	$arrSystem[2] = "APODERADO SND";
	$arrSystem[9] = "MATEONET";

	$snd=0;
	$mateo=0;
	$aposnd =0;
	foreach ($arrUser as $hora => $sysdata){

		$snd = ($sysdata[1])?$sysdata[1]:0;
		$mateo = ($sysdata[9])?$sysdata[9]:0;
		$aposnd = ($sysdata[2])?$sysdata[2]:0;

		$arrRes[] = "['". $hora ."', ". $snd .", ". $mateo .", ".$aposnd."]";
		$arrRes2[] = "{ hora: '". $hoy." ".$hora ."', napsis: ". $snd .", mateonet: ". $mateo .", apoderados:  ".$aposnd."}";
	}

	$rs["RES1"] = $arrRes;
	$rs["RES2"] = $arrRes2;
// 			echo "<pre>";
// 			print_r($rs);
// 			echo "</pre>";
	return $rs;
}

function asterisk($hoy,$seg = 30){

	$link =  mysqli_connect('192.168.7.31', 'root', 'tec.wor_08');
	mysqli_select_db($link,'asteriskcdrdb');

	$data = array();

	$sql1 = 'SELECT
				HOUR(c.calldate) as "HORA",
				c.disposition as "DISP",
				SUM(c.duration) AS SUMA,
				COUNT(*) as CANT
			FROM asteriskcdrdb.cdr as c
			LEFT JOIN asterisk.users as u ON u.extension = c.dst
			WHERE DATE(c.calldate) = curdate()
			AND c.dst in ( 413,414,416,425,427,458)
			AND LENGTH(c.src) > 3
			AND c.duration != 0
			GROUP BY HOUR(c.calldate),c.disposition
			ORDER BY c.calldate ASC;';
	$res = mysqli_query($link, $sql1);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$suma[$row["HORA"]]["ALL"] = $suma[$row["HORA"]]["ALL"] + $row["CANT"];
		if($row["DISP"] == "ANSWERED"){
			$suma[$row["HORA"]]["ANSWERED"] = $row["CANT"];
		}else{
			$suma[$row["HORA"]]["NOANSWERED"] = $suma[$row["HORA"]]["NOANSWERED"] + $row["CANT"];
		}

	}

	foreach ($suma as $hora => $dd){

		if(!$dd["ANSWERED"]){
			$ANSWERED = 0;
		}else{
			$ANSWERED = $dd["ANSWERED"];
		}

		if(!$dd["NOANSWERED"]){
			$NOANSWERED = 0;
		}else{
			$NOANSWERED = $dd["NOANSWERED"];
		}


		$arrData[] = "{ y: '".$hora."', a: ".$dd["ALL"].", b:".$NOANSWERED.", c: ".$ANSWERED." }";

	}


	$data["ALL_CALL"] = implode(",", $arrData) ;



	$sql2 = 'SELECT
					count(*) as llamadas, c.disposition
				FROM asteriskcdrdb.cdr as c
				INNER JOIN asterisk.users as u ON u.extension = c.dst
				WHERE DATE(c.calldate) = curdate()
				AND c.dst in (413,414,416,425,427,458)
				AND LENGTH(c.src) > 3
				AND c.duration != 0
					group by c.disposition
				 order by c.calldate desc;';
	$res = mysqli_query($link, $sql2);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data["COUNT_LLAMADAS"][$row["disposition"]] = $row["llamadas"];
	}


	$sql3 = "SELECT
				c.calldate as 'Fecha',
				c.src as 'Desde',
				c.dst as 'Hacia',
				SUM(c.duration)/60 as 'Minutos',
				count(*) as 'Total',
				ROUND(avg(c.duration)/60,1) as 'Promedio',
				if(perdidas.cantLlamadasPerdidas is null,0,perdidas.cantLlamadasPerdidas) as cantLlamadasPerdidas,
				c.disposition as 'Disponibilidad',
				u.name as 'Nombres'
				FROM asteriskcdrdb.cdr as c
				LEFT JOIN asterisk.users as u ON u.extension = c.dst
			    LEFT JOIN (
                SELECT	u.extension,count(u.extension) as 'cantLlamadasPerdidas'
			FROM asteriskcdrdb.cdr as c
             INNER JOIN asterisk.users as u ON u.extension = c.dst
             WHERE DATE(c.calldate) = curdate()
            AND c.dst in (413,414,416,425,427,458)
            AND LENGTH(c.src) > 3
            AND c.duration != 0
			AND c.disposition  LIKE 'NO ANSWER'
			group by u.extension
                ) as perdidas on perdidas.extension = u.extension
				WHERE DATE(c.calldate) = curdate()
				AND c.dst in (413,414,416,425,427,458)
				AND LENGTH(c.src) > 3
				AND c.billsec >= '".$seg."' AND c.disposition = 'ANSWERED'
				GROUP BY c.dst ORDER BY AVG(c.billsec) ASC, SUM(c.billsec) ASC, count(*) ASC  LIMIT 0,10";
	$res = mysqli_query($link, $sql3);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data["TOP"][] = $row;
	}

	return $data;

}

function issuesOps($opcionMesa){
	$data = array('total' => '0');
	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');


		$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}

	$sql1 = 'SELECT
			 `prj`.`prj_title` AS `Nombre Proyecto`,
			 `ud`.`usr_full_name` AS `ASIGNADO_A`,
			 count(iss.iss_id) as cantidad,
			 group_concat(iss.iss_id) as "nro_issues"
			 FROM
			 `eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
			 LEFT JOIN `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
			 LEFT JOIN `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
			 LEFT JOIN `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
			 LEFT JOIN `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
			 LEFT JOIN `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
			 LEFT JOIN `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
			 WHERE
			 `iss`.`iss_private` = 0
			  '.$whereMesa.'
			 AND  `st`.`sta_id` = 10
			 GROUP BY `prj`.`prj_id`,`ud`.`usr_id`
			 ORDER BY count(iss.iss_id) desc;';
	$res = mysqli_query($link, $sql1);
	//print_r($sql1);exit;
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['total'] = $data["total"] + $row["cantidad"];
		$data['tabla'][] = $row;
	}

	return $data;

}

function TotalIssuesDevueltosOps($opcionMesa) {
        $data = array('total' => '0');
        $link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
        mysqli_select_db($link,'eventum');


                $whereMesa = '';
        if($opcionMesa == 'edu')
        {
                $whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
        }
        else
        {
                if($opcionMesa == 'salud')
                {
                        $whereMesa = "AND (`prj`.`prj_id`  = 15)";
                }
        }

        $sql1 = 'SELECT count(distinct(iss.iss_id)) as cantidad
                         FROM
                         `eventum`.`issue` `iss`
                        LEFT JOIN `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
                         LEFT JOIN `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
                         LEFT JOIN `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
                         LEFT JOIN `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
                         LEFT JOIN `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
                         LEFT JOIN `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
                         LEFT JOIN `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
                         WHERE
                         `iss`.`iss_private` = 0
                          '.$whereMesa.'
                         AND  `st`.`sta_id` = 10 ;';
        $res = mysqli_query($link, $sql1);
        //print_r($sql1);exit;
        while ( $row = mysqli_fetch_array ( $res) ) {

                $data['total'] = $data["total"] + $row["cantidad"];
                $data['tabla'][] = $row;
        }

        return $data;

}

function issuesCliente($opcionMesa){
        $link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
        mysqli_select_db($link,'eventum');

        $data = array();
        $whereMesa = '';
        if($opcionMesa == 'edu')
        {
                $whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
        }
        else
        {
                if($opcionMesa == 'salud')
                {
                        $whereMesa = "AND (`prj`.`prj_id`  = 15)";
                }
        }
        $sql1 = 'SELECT count(iss.iss_id) as cantidadCliente
                         FROM
                         ((((((((((`eventum`.`issue` `iss`
                         LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
                         LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
                         LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
                         LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
                         LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
                         LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
                         LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
                         AND (`seccion`.`icf_fld_id` = 4))))
                         LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
                         AND (`rbd`.`icf_fld_id` = 1))))
                         LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
                         AND (`seccion_option`.`cfo_fld_id` = 4))))
                         LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
                         WHERE
                         `iss`.`iss_private` = 0
                          '.$whereMesa.'
                         AND  `st`.`sta_id` = 9;';
        $res = mysqli_query($link, $sql1);

        while ( $row = mysqli_fetch_array ( $res) ) {

                $data['totalCliente'] = $row["cantidadCliente"] ;
        }
        return $data;

}

function issuesSac($opcionMesa){
	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}
	$sql1 = 'SELECT count(iss.iss_id) as cantidadSAC
			 FROM
			 ((((((((((`eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
			 LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
			 LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
			 LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
			 LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
			 LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
			 LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
			 AND (`seccion`.`icf_fld_id` = 4))))
			 LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
			 AND (`rbd`.`icf_fld_id` = 1))))
			 LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
			 AND (`seccion_option`.`cfo_fld_id` = 4))))
			 LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
			 WHERE
			 `iss`.`iss_private` = 0
			  '.$whereMesa.'
			 AND  `st`.`sta_id` = 4;';
	$res = mysqli_query($link, $sql1);

	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['totalSAC'] = $row["cantidadSAC"] ;
	}
	return $data;

}

function issuesCerrados($opcionMesa){


$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}
	$sql1 = 'SELECT  count(iss_id) as cantidad FROM ((((((((((`eventum`.`issue` `iss`
			LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
			LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
			LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
			LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
			LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
			LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
			LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
			AND (`seccion`.`icf_fld_id` = 4))))
			LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
			AND (`rbd`.`icf_fld_id` = 1))))
			LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
			AND (`seccion_option`.`cfo_fld_id` = 4))))
			LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
			WHERE `iss`.`iss_private` = 0 AND (`prj`.`prj_id` NOT IN (3 , 4)) AND  `st`.`sta_id` = 5
             AND iss_closed_date BETWEEN NOW() - INTERVAL (day(NOW())-1) DAY AND ADDDATE(LAST_DAY(CURDATE()), 1)
             AND ud.usr_id IN (111,142,87,20) ORDER BY `prj`.`prj_id`,`ud`.`usr_full_name` ASC;';
	$res = mysqli_query($link, $sql1);


	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['totalSAC1'] = $row["cantidad"];
	}
//    echo "<pre>";
 //   print_r($data);
//    echo "</pre>";
	return $data;



}


function issuesErrorProyecto($opcionMesa){

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array('Total' => '0');
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}
	$sql1 = 'SELECT  `prj`.`prj_title` AS `Nombre_Proyecto`,count(distinct(iss.iss_id)) as cantidad
			 FROM
			 `eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
			 LEFT JOIN `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
			 LEFT JOIN `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
			 LEFT JOIN `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
			 LEFT JOIN `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
			 LEFT JOIN `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
			 LEFT JOIN `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
			 WHERE
			 `iss`.`iss_private` = 0
			  '.$whereMesa.'
			 AND  `st`.`sta_id` not in(5,6)
			  AND pc.prc_title = "Error (Bug)"
			 group by prj.prj_id
			 order by count(distinct(iss.iss_id)) desc;';
	$res = mysqli_query($link, $sql1);

	//print_r($sql1);exit;
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['Error'][] = $row ;
		$data['Total'] = $data["Total"] + $row["cantidad"];
	}

    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
	return $data;

}

function issuesHaciendo($opcionMesa){

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}
	$sql1 = 'SELECT
			 `prj`.`prj_title` AS `Nombre Proyecto`,
			 `ud`.`usr_full_name` AS `ASIGNADO_A`,
			 count(iss.iss_id) as cantidad,
			 group_concat(if(his_created_date is not null,concat(iss.iss_id,"<br>",if((time_to_sec((timediff(now() + interval 1 hour,his_created_date - interval 3 hour))))/(60*60*24)>1,concat(round((time_to_sec((timediff(now() + interval 1 hour,his_created_date - interval 3 hour))))/(60*60*24),2)," días"),timediff(now() + interval 1 hour,his_created_date - interval 3 hour))),iss.iss_id)) as issues
			 FROM
			 `eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
			 LEFT JOIN `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
			 LEFT JOIN `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
			 LEFT JOIN `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
			 LEFT JOIN `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
			 LEFT JOIN `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
			 LEFT JOIN `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
			 LEFT JOIN `eventum`.`issue_history` h on h.his_iss_id = iss.iss_id AND (h.his_summary like "%-> implementation%" or h.his_summary like "%implementation%")
			 WHERE
			 `iss`.`iss_private` = 0
			 '.$whereMesa.'
			 AND `st`.`sta_id` = 3
			 GROUP BY `prj`.`prj_id`,`ud`.`usr_id`
			 ORDER BY count(iss.iss_id) desc;';
	$res = mysqli_query($link, $sql1);
	//print_r($sql1);exit;
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['Haciendo'][] = $row ;
	}
    	return $data;
}

function issuesCerradosDia($opcionMesa){
	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
        mysqli_select_db($link,'eventum');

        $data = array();
        $whereMesa = '';
        if($opcionMesa == 'edu')
        {
                $whereMesa = "AND (`iss`.`iss_prj_id` NOT IN (3,4,5,6,14,15))";
        }
        else
        {
                if($opcionMesa == 'salud')
                {
                        $whereMesa = "AND (`iss`.`iss_prj_id`  = 15)";
                }
        }
        $sql1 = 'SELECT count(distinct(iss.iss_id)) as cantidadCerradosDia
                         FROM
                         `eventum`.`issue` `iss`
                         WHERE
                         `iss`.`iss_private` = 0
			 '.$whereMesa.'
                         AND  iss.iss_sta_id = 5
                         AND date(now()) = date(iss.iss_closed_date);';
	 $res = mysqli_query($link, $sql1);

        while ( $row = mysqli_fetch_array ( $res) ) {

                $data = $row ;
        }
	return $data;
}

function issuesCerradosSemana($opcionMesa){
        $link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
        mysqli_select_db($link,'eventum');

        $data = array();
        $whereMesa = '';
        if($opcionMesa == 'edu')
        {
                $whereMesa = "AND (`iss`.`iss_prj_id` NOT IN (3,4,5,6,14,15))";
        }
        else
        {
                if($opcionMesa == 'salud')
                {
                        $whereMesa = "AND (`iss`.`iss_prj_id`  = 15)";
                }
        }
        $sql1 = 'SELECT  count(distinct(iss.iss_id)) as cantidadCerradosSemana
                         FROM
                         `eventum`.`issue` `iss`
                         WHERE
                         `iss`.`iss_private` = 0
                          '.$whereMesa.'
                         AND  iss.iss_sta_id = 5
                         AND year(now()) = year(iss.iss_closed_date)
                         AND weekofyear(now()) = weekofyear(iss.iss_closed_date);';
        $res = mysqli_query($link, $sql1);

        while ( $row = mysqli_fetch_array ( $res) ) {

                $data = $row ;
        }

        return $data;

}

function issuesCerradosMes($opcionMesa){
        $link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
        mysqli_select_db($link,'eventum');

        $data = array();
        $whereMesa = '';
        if($opcionMesa == 'edu')
        {
                $whereMesa = "AND (`iss`.`iss_prj_id` NOT IN (3,4,5,6,14,15))";
        }
        else
        {
                if($opcionMesa == 'salud')
                {
                        $whereMesa = "AND (`iss`.`iss_prj_id`  = 15)";
                }
        }
        $sql1 = 'SELECT  count(distinct(iss.iss_id)) as cantidadCerradosMes
                         FROM
                         `eventum`.`issue` `iss`
                         WHERE
                         `iss`.`iss_private` = 0
                          '.$whereMesa.'
                         AND  iss.iss_sta_id = 5
                         AND year(now()) = year(iss.iss_closed_date)
                         AND month(now()) = month(iss.iss_closed_date);';
        $res = mysqli_query($link, $sql1);

        while ( $row = mysqli_fetch_array ( $res) ) {

                $data = $row ;
        }

        return $data;

}


function issuesCerradosAno($opcionMesa){


$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}




	$sql1 = 'SELECT  count(iss_id) as cantidad
			FROM
			((((((((((`eventum`.`issue` `iss`
			LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
			LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
			LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
			LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
			LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
			LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
			LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
			AND (`seccion`.`icf_fld_id` = 4))))
			LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
			AND (`rbd`.`icf_fld_id` = 1))))
			LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
			AND (`seccion_option`.`cfo_fld_id` = 4))))
			LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
			WHERE
			`iss`.`iss_private` = 0
			 AND (`prj`.`prj_id` NOT IN (3 , 4))
			AND  `st`.`sta_id` = 5
             AND iss_closed_date between concat(YEAR(now() - interval (day(now())-1) day),"-","01","-","01") and CONCAT(YEAR(now() - interval (day(now())-1) day),"-","12","-","31")
             and ud.usr_id in(20,111,142,87)
			#group by `ud`.`usr_full_name`
			order by `prj`.`prj_id`,`ud`.`usr_full_name`  ASC;';



	$res = mysqli_query($link, $sql1);



	while ( $row = mysqli_fetch_array ( $res) ) {



		$data['totalAno'] = $row["cantidad"];

	}

	return $data;



}
/*

function issuesCerradosDia($opcionMesa){


$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db('eventum',$link);

	$data = array();
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}




	$sql1 = 'SELECT  count(iss_id) as cantidad
			FROM
			((((((((((`eventum`.`issue` `iss`
			LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
			LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
			LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
			LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
			LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
			LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
			LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
			AND (`seccion`.`icf_fld_id` = 4))))
			LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
			AND (`rbd`.`icf_fld_id` = 1))))
			LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
			AND (`seccion_option`.`cfo_fld_id` = 4))))
			LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
			WHERE
			`iss`.`iss_private` = 0
			 AND (`prj`.`prj_id` NOT IN (3 , 4))
			AND  `st`.`sta_id` = 5
             AND DATE(iss_closed_date) = CURDATE()
             and ud.usr_id in(20,111,142,87)
			#group by `ud`.`usr_full_name`
			order by `prj`.`prj_id`,`ud`.`usr_full_name`  ASC;';



	$res = mysqli_query($link, $sql1);



	while ( $row = mysqli_fetch_array ( $res) ) {



		$data['totalDia'] = $row["cantidad"];


	}

	return $data;



}

*/

function PlazoIssues(){

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();

		$sql1 = 'SELECT
				 `prj`.`prj_title` AS `Nombre_Proyecto`,
				 `iss`.`iss_id` AS `ID`,
				 iss.iss_summary `RESUMEN`,
				 iss.iss_description `CONTENIDO_ISSUE`,
				 `st`.`sta_title` AS `ESTADO`,
				 `iss`.`iss_sta_id`,
				 `pc`.`prc_title` AS `CATEGORIA`,
				 `u`.`usr_full_name` AS `CREADO_POR`,
				 `u`.`usr_email` as `email_mesa`,
				 `ud`.`usr_full_name` AS `ASIGNADO_A`,
				 `ud`.`usr_email` as `email_operaciones`,
				  iss.iss_expected_resolution_date as `Fecha_Comprometida`,
				  DATEDIFF(iss.iss_expected_resolution_date,date(now())) as `dias_restantes`
				 FROM
				 ((((((((((`eventum`.`issue` `iss`
				 LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
				 LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
				 LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
				 LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
				 LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
				 LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
				 LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
				 AND (`seccion`.`icf_fld_id` = 4))))
				 LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
				 AND (`rbd`.`icf_fld_id` = 1))))
				 LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
				 AND (`seccion_option`.`cfo_fld_id` = 4))))
				 LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
				 WHERE
				 `iss`.`iss_private` = 0
				  AND (`prj`.`prj_id` NOT IN (3 , 4, 6))
				  AND `iss`.`iss_sta_id` not in (5,6,9)
				 AND  iss.iss_expected_resolution_date != ""
				 order by iss.iss_expected_resolution_date asc;';

	$res = mysqli_query($link, $sql1);

	//print_r($sql1);exit;
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['PlazoIssues'][] = $row ;
	}

    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
	return $data;

}


function issuesErrorSeccion()
{

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();

		$sql1 = 'SELECT  seccion_option.`cfo_value` AS `SECCION`,count(iss.iss_id) as cantidad,group_concat(iss.iss_id)
			 FROM
			 `eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
			 LEFT JOIN `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
			 LEFT JOIN `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
			 LEFT JOIN `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
			 LEFT JOIN `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
			 LEFT JOIN `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
			 LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON `seccion`.`icf_iss_id` = `iss`.`iss_id`
			 AND `seccion`.`icf_fld_id` = 4
			 LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON `rbd`.`icf_iss_id` = `iss`.`iss_id`
			 AND `rbd`.`icf_fld_id` = 1
			 LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON `seccion_option`.`cfo_id` = `seccion`.`icf_value`
			 AND `seccion_option`.`cfo_fld_id` = 4
			 LEFT JOIN `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
			 WHERE
			 `iss`.`iss_private` = 0
			  AND `prj`.`prj_id` = 1
			 AND  `st`.`sta_id` not in(5,6)
			  AND pc.prc_title = "Error (Bug)"
			 group by prj.prj_id,`seccion_option`.`cfo_value`
             having cantidad > 2 or SECCION like "%Libro de clases"
			 order by count(iss.iss_id) desc;';

	$res = mysqli_query($link, $sql1);

	//print_r($sql1);exit;
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['PlazoIssues'][] = $row ;
	}

    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
	return $data;
}

function IssuesTopSecciones()
{

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();

		$sql1 = 'SELECT  SUBSTRING_INDEX(`seccion_option`.`cfo_value`,"-",-1) as `Modulo`,count(distinct(iss.iss_id)) as Cantidad,SUBSTR(group_concat(distinct(iss.iss_id)),1,17) as Issues
			 FROM
			 `eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
			 LEFT JOIN `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
			 LEFT JOIN `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
			 LEFT JOIN `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
			 LEFT JOIN `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
			 LEFT JOIN `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
			 LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON `seccion`.`icf_iss_id` = `iss`.`iss_id`
			 AND `seccion`.`icf_fld_id` = 4
			 LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON `rbd`.`icf_iss_id` = `iss`.`iss_id`
			 AND `rbd`.`icf_fld_id` = 1
			 LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON `seccion_option`.`cfo_id` = `seccion`.`icf_value`
			 AND `seccion_option`.`cfo_fld_id` = 4
			 LEFT JOIN `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
			 WHERE
			 `iss`.`iss_private` = 0
			  AND `prj`.`prj_id` = 1
			 AND  `st`.`sta_id` not in (5,6)
			  AND pc.prc_title = "Error (Bug)"
			 group by prj.prj_id,SUBSTRING_INDEX(`seccion_option`.`cfo_value`,"-",-1)
             having cantidad >= 5 or (Modulo like "%Libro%" OR Modulo like "%Recaudaci%" OR Modulo like "%Informe%")
			 order by count(distinct(iss.iss_id)) desc, Modulo asc;';

	$res = mysqli_query($link, $sql1);

	//print_r($sql1);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['IssuesTopSecciones'][] = $row ;
	}

    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
	return $data;
}
function CreadosAnoAnterior(){
        $link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
        mysqli_select_db($link,'eventum');

        $data = array();

                $sql1 = 'SELECT issues_error_mes_actual.`Nombre_Proyecto`,concat(issues_error_mes_actual.CantidadIssues,"/",issues_error_mes_anterior.CantidadIssues) as actualVSanterior,issues_error_mes_actual.CantidadIssues/issues_error_mes_anterior.CantidadIssues*100 as "RelacionAnoAnterior" FROM
(
	SELECT IDPrj,`Nombre_Proyecto`,count(IDPrj) as CantidadIssues
	FROM eventum.issues
	where CATEGORIA = "Error (Bug)"
	AND `ANO_CREACION` = year(now())
	AND MES_CREACION = month(now())
	AND IDPrj != 15
	GROUP BY IDPrj
) AS issues_error_mes_actual
left join
(
	SELECT IDPrj,`Nombre_Proyecto`,count(IDPrj) as CantidadIssues
	FROM eventum.issues
	where CATEGORIA = "Error (Bug)"
	AND `ANO_CREACION` = year(now())-1
	AND MES_CREACION = month(now())
	AND IDPrj != 15
	GROUP BY IDPrj
) AS issues_error_mes_anterior on issues_error_mes_actual.IDPrj = issues_error_mes_anterior.IDPrj;';

        $res = mysqli_query($link, $sql1);

        //print_r($sql1);
        while ( $row = mysqli_fetch_array ( $res) ) {

                //print_r($res);
		$data['IssuesCreadosAnoAnterior'][] = $row ;
        }

//    echo "<pre>";
//    print_r($data);
//    echo "</pre>";
        return $data;

}

function CreaURL(){
        $link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
        mysqli_select_db($link,'eventum');

        $data = array();

                $sql1 = 'SELECT
        `prj`.`prj_id` AS `IDPrj`,
        `prj`.`prj_title` AS `Nombre_Proyecto`,
        `iss`.`iss_id` AS `ID`,
        `st`.`sta_title` AS `ESTADO`,
        `pc`.`prc_title` AS `CATEGORIA`,
        datediff(date(now()),`iss`.`iss_created_date`) as diff

    FROM
        `issue` `iss`
        LEFT JOIN `status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
        LEFT JOIN `project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
        LEFT JOIN `project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
    WHERE   `prj`.`prj_id` = 1
			AND `iss`.`iss_private` = 0

            AND `prj`.`prj_id` NOT IN (3 , 4)
            AND `iss`.`iss_sta_id` NOT IN (5,6)
            AND `iss`.`iss_prc_id` IN (36,130)
    GROUP BY `iss`.`iss_id`
    ORDER BY `iss`.`iss_id`;';

        $res = mysqli_query($link, $sql1);

        //print_r($sql1);
        while ( $row = mysqli_fetch_array ( $res) ) {

                //print_r($res);
                $data['IssuesCreaURL'][] = $row ;
        }

//    echo "<pre>";
//    print_r($data);
//    echo "</pre>";
        return $data;

}

function CerradosCreados($opcionMesa){

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "AND (`prj`.`prj_id`  = 15)";
		}
	}
	$sql1 = "select anual.`prj_title`,
concat(cerrados_ano,'/',pendientes_total) as detAnual,ROUND(cerrados_ano/pendientes_total*100,2) as Anual,
concat(cerrados_mes,'/',creados_mes) as detMensual,ROUND(cerrados_mes/creados_mes*100,2) as Mensual,
concat(cerrados_semana,'/',creados_semana) as detSemanal,ROUND(cerrados_semana/creados_semana*100,2) as Semanal from

(
SELECT
   count(iss.iss_closed_date) as cerrados_ano,`prj`.`prj_id`,`prj`.`prj_title`
FROM
    `eventum`.`issue` `iss`
        LEFT JOIN
    `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
        LEFT JOIN
    `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
        LEFT JOIN
    `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
        LEFT JOIN
    `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
        LEFT JOIN
    `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
        LEFT JOIN
    `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
        LEFT JOIN
    `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
WHERE
    `iss`.`iss_private` = 0
       ".$whereMesa."
        AND year(iss.iss_closed_date) = year(now())
        group by `prj`.`prj_id`
) as anual
left join
(
SELECT
   count(iss.iss_closed_date) as cerrados_mes,`prj`.`prj_id`,`prj`.`prj_title`
FROM
    `eventum`.`issue` `iss`
        LEFT JOIN
    `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
        LEFT JOIN
    `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
        LEFT JOIN
    `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
        LEFT JOIN
    `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
        LEFT JOIN
    `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
        LEFT JOIN
    `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
        LEFT JOIN
    `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
WHERE
    `iss`.`iss_private` = 0
        ".$whereMesa."
        AND year(iss.iss_closed_date) = year(now())
        AND month(iss.iss_closed_date) = month(now())
        group by `prj`.`prj_id`
) as mes on anual.prj_id = mes.prj_id
left join
(
SELECT
   count(iss.iss_created_date) as cerrados_semana,`prj`.`prj_id`,`prj`.`prj_title`
FROM
    `eventum`.`issue` `iss`
        LEFT JOIN
    `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
        LEFT JOIN
    `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
        LEFT JOIN
    `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
        LEFT JOIN
    `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
        LEFT JOIN
    `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
        LEFT JOIN
    `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
        LEFT JOIN
    `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
WHERE
    `iss`.`iss_private` = 0
        ".$whereMesa."
        AND year(iss.iss_closed_date) = year(now())
        AND month(iss.iss_closed_date) = month(now())
        AND weekofyear(iss.iss_closed_date) = weekofyear(now())
        group by `prj`.`prj_id`
) as semana on anual.prj_id = semana.prj_id
left join
(
SELECT
   count(iss.iss_id) as pendientes_total,`prj`.`prj_id`,`prj`.`prj_title`
FROM
    `eventum`.`issue` `iss`
        LEFT JOIN
    `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
        LEFT JOIN
    `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
        LEFT JOIN
    `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
        LEFT JOIN
    `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
        LEFT JOIN
    `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
        LEFT JOIN
    `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
        LEFT JOIN
    `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
WHERE
    `iss`.`iss_private` = 0
        ".$whereMesa."
        AND `pc`.`prc_title` not like '%funcionalidad%'
        AND iss.iss_closed_date is null
        AND `iss`.`iss_sta_id` not in (5,6)
        group by `prj`.`prj_id`
) as anual_creados on anual.prj_id = anual_creados.prj_id
left join
(
SELECT
   count(iss.iss_created_date) as creados_mes,`prj`.`prj_id`,`prj`.`prj_title`
FROM
    `eventum`.`issue` `iss`
        LEFT JOIN
    `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
        LEFT JOIN
    `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
        LEFT JOIN
    `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
        LEFT JOIN
    `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
        LEFT JOIN
    `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
        LEFT JOIN
    `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
        LEFT JOIN
    `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
WHERE
    `iss`.`iss_private` = 0
        ".$whereMesa."
        AND year(iss.iss_created_date) = year(now())
        AND month(iss.iss_created_date) = month(now())
        group by `prj`.`prj_id`
) as mes_creados on anual.prj_id = mes_creados.prj_id
left join
(
SELECT
   count(iss.iss_created_date) as creados_semana,`prj`.`prj_id`,`prj`.`prj_title`
FROM
    `eventum`.`issue` `iss`
        LEFT JOIN
    `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
        LEFT JOIN
    `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
        LEFT JOIN
    `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
        LEFT JOIN
    `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
        LEFT JOIN
    `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
        LEFT JOIN
    `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
        LEFT JOIN
    `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
WHERE
    `iss`.`iss_private` = 0
        ".$whereMesa."
        AND year(iss.iss_created_date) = year(now())
        AND month(iss.iss_created_date) = month(now())
        AND weekofyear(iss.iss_created_date) = weekofyear(now())
        group by `prj`.`prj_id`
) as semana_creados on anual.prj_id = semana_creados.prj_id
ORDER BY Anual asc;";
	$res = mysqli_query($link, $sql1);

	//print_r($sql1);exit;
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['CreadosCerrados'][] = $row ;
	}

    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
	return $data;

}
