<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
setlocale(LC_ALL,"es_ES");
include_once 'funciones.php';

$usuariosConectados = UsuariosConectados();
if (array_key_exists('mesa', $_REQUEST))
{
    $opcionMesa = $_REQUEST["mesa"];
}
else
{
    $opcionMesa = 'all';
}

$issuesOperaciones = getIssuesDevueltosOperaciones($opcionMesa);
$TotalIssuesDevuOps = getTotalIssuesDevueltosOps($opcionMesa);
$issuesSAC = getIssuesSAC($opcionMesa);
$issuesErrorProyecto = getIssuesErrorProyecto($opcionMesa);
$issuesTopSecciones = getIssuesTopSecciones($opcionMesa);
$issuesCerradosHechos = getissuescerradoshechos($opcionMesa);
//$issuesCerradosCreados = getCerradosCreados($opcionMesa);
$issuesCerrados = getIssuesCerrados($opcionMesa);
$issuesHaciendo = getIssuesHaciendo($opcionMesa);
$issuesXCreadosVSAnoAnterior = getIssuesCreadosVSAnoAnterior($opcionMesa);
$issuesURL = getIssuesCreaURL();
$issuesCliente = getIssuesCliente($opcionMesa);
//$issuesCerradoDia = getIssuesCerradosDia($opcionMesa);
//$issuesCerradosAno = getIssuesCerradosAno($opcionMesa);
//echo "<pre>";
//print_r($issuesURL);
//echo "</pre>";
$alertURL=false;

if (!empty($issuesURL))
{
	if(is_array($issuesURL))
	{
		foreach ($issuesURL as $index => $datoIssueURL)
		{
			foreach ($datoIssueURL as $index2 => $datoIssueURLx)
			{
				//$mensaje = "<br>Tenemos una URL pendiente hace ".$issuesURL["IssuesCreaURL"][0]['diff']. ' días ('.$issuesURL["IssuesCreaURL"][0]['ID'].')';
				if($datoIssueURLx['ESTADO'] == 'discovery')
				{
					$alertURL = true;
					$mensaje .= "<br>Tenemos una URL pendiente hace ".$datoIssueURLx['diff']. ' días (Issue: '.$datoIssueURLx['ID'].') en estado '.$datoIssueURLx['ESTADO'];
				}
				else {
				$mensaje .= "<br>Tenemos una URL pendiente hace ".$datoIssueURLx['diff']. ' días (Issue: '.$datoIssueURLx['ID'].') en estado '.$datoIssueURLx['ESTADO'];
				}
			}
		}

$audio = '<audio autoplay src="sonidos/url.mp3"></audio>';
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <META HTTP-EQUIV="REFRESH" CONTENT="60;url=https://mesados.inexoos.com" />
    <title>Mesa 2 | Operaciones Inexoos</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">


        <div id="page-wrapper" style="margin: 10px 0px;">
            <div class="row" style="margin-top: -20px;">
                <div class="col-lg-14">
                    <h1 class="page-header">Indicadores de Operaciones al <?php echo utf8_encode(strftime("%A, %d de %B de %Y"));
if (!empty($issuesURL))
{
	echo $mensaje;
	if ($alertURL)
	{
		echo $audio;
	}
}
 ?>
                    </h1>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="card card-inverse card-success">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3">

                                  <!--  <i class="fa fa-comments fa-5x"></i>-->
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="font-size:45px;" >
<span class="badge badge-pill badge-default" style="font-size: 45px;"><?php echo $issuesCerrados['Dia'];?></span>
<span class="badge badge-pill badge-default" style="font-size: 45px;"><?php echo $issuesCerrados['Semana'];?></span>
<span class="badge badge-pill badge-default" style="font-size: 45px;"><?php echo $issuesCerrados['Mes'];?></span>

				   </div>
                                    <div style="font-size:40px;" align="center"><b>Issues Cerrados</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-inverse card-danger">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="font-size:50px;" ><?php echo ($TotalIssuesDevuOps['total'] > 0)?$TotalIssuesDevuOps['total']:0?></div>
                                    <div style="font-size:40px;" align="center"><b>Operaciones</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-inverse card-warning">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="../img/mesa2.png"  height="85px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="font-size:50px;"><b><?php echo $issuesSAC['totalSAC']; ?>+<?php echo $issuesCliente['totalCliente']; ?></b></div>
                                    <div style="font-size:40px;" align="center"><b>SAC+Cli</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-inverse card-primary">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="../img/bug-xxl.png"  height="65px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="font-size:50px;"><?php echo $issuesErrorProyecto['Total'];?></div>
                                    <div style="font-size:40px;" align="center"><b>Pendientes Error</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div id="demo" class="carousel slide" data-ride="carousel" data-interval="10000" >

                      <!-- Indicators -->
                      <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                      </ul>

                      <!-- The slideshow -->
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                    <div class="card card-default">
                        <div class="card-header">
                           <h3 class="card-title"><i class="fa fa-bar-chart-o fa-fw"></i> Issues devueltos a Operaciones</h3>
                        </div>
                        <!-- /.card-heading -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-16">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <?php
                                            $x = 1;
                                            foreach ($issuesOperaciones["tabla"] as $index => $datos){
                                            if($datos['cantidad'] >=0 && $datos['cantidad'] <= 1)
                                                $estadoColOps = 'success';
                                            if($datos['cantidad'] >=2 && $datos['cantidad'] <= 4)
                                                $estadoColOps = 'warning';
                                            if($datos['cantidad'] >=5)
                                                $estadoColOps = 'danger';
                                            $arrIngOps = explode(' ',$datos['ASIGNADO_A']);
                                            if(utf8_encode($datos['Nombre Proyecto']) == 'Napsis SND')
                                                $datos['Nombre Proyecto'] = 'SND';
                                            if(utf8_encode($datos['Nombre Proyecto']) == 'Portal Psicométrico')
                                                $datos['Nombre Proyecto'] = 'PPN';
                                            if(utf8_encode($datos['Nombre Proyecto']) == 'MateoNet')
                                                $datos['Nombre Proyecto'] = 'MNT';

                                            ?>
                                            <tbody>
                                                <tr class="table-<?php echo $estadoColOps?>">
                                                    <td class="alert alert-<?php echo $estadoColOps?>" style="font-size:40px;"><b><?php echo utf8_encode($arrIngOps[0]); ?></td>
                                                    <td class="alert alert-<?php echo $estadoColOps?>" style="font-size:40px;"><b><?php echo utf8_encode($datos['Nombre Proyecto']);?></td>
                                                    <td class="alert alert-<?php echo $estadoColOps?>" style="font-size:40px;"><b><?php echo $datos['cantidad']; ?></td>
                                                    <td class="alert alert-<?php echo $estadoColOps?>" style="font-size:40px;"><b>
                                                        <?php
                                                        $arrIssues = explode(',',$datos['nro_issues']);
                                                        foreach ($arrIssues as $key => $nro_issue) {
                                                        ?>
                                                        <span class="badge badge-pill badge-<?php echo $estadoColOps?>" style='font-size: 30px;' ><?php echo $nro_issue; ?></span>
                                                        <?php
                                                            }
                                                        ?>
                                                        </td>
                                                </tr>
                                            </tbody>

                                            <?php } ?>

                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->

                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
<div class="carousel-item">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-bar-chart-o fa-fw"></i> Issues de Error Pendientes</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-16">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <?php
                                            $x = 1;
                                            foreach ($issuesErrorProyecto['Error'] as $index => $datos){
                                            if($datos['cantidad'] >=0 && $datos['cantidad'] <= 9)
                                                {$estadoColOps = 'success';$color = "#3c763d";}
                                            if($datos['cantidad'] >=10 && $datos['cantidad'] <= 15)
                                                {$estadoColOps = 'warning';$color="#8a6d3b";}
                                            if($datos['cantidad'] >=16)
                                                {$estadoColOps = 'danger';$color="#a94442";}
                                            ?>
                                            <tbody>
                                                <tr class="table-<?php echo $estadoColOps?>">
                                                    <td style="font-size:40px;color:<?php echo $color ?>"><b><?php echo utf8_encode($datos['Nombre_Proyecto']);?></b></td>
                                                    <td class="alert alert-<?php echo $estadoColOps?>" style="font-size:33px;"><b><span class="badge badge-pill badge-<?php echo $estadoColOps?>" style='font-size: 45px;' ><?php echo $datos['cantidad']; ?></span></td>
                                                </tr>
                                            </tbody>

                                            <?php } ?>

                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->

                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>

</div>

<div class="carousel-item">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-bar-chart-o fa-fw"></i> Issues de Error por Módulo</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-16">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <?php
                                            $x = 1;
                                            foreach ($issuesTopSecciones['IssuesTopSecciones'] as $index => $datos){
                                            if($datos['Cantidad'] >=0 && $datos['Cantidad'] <= 4)
                                                {$estadoColOps = 'success';$color = "#3c763d";}
                                            if($datos['Cantidad'] >=5 && $datos['Cantidad'] <= 9)
                                                {$estadoColOps = 'warning';$color="#8a6d3b";}
                                            if($datos['Cantidad'] >=10)
                                                {$estadoColOps = 'danger';$color="#a94442";}
                                            ?>
                                            <tbody>
                                                <tr class="table-<?php echo $estadoColOps?>">
                                                    <td style="font-size:40px;color:<?php echo $color ?>"><b><?php echo utf8_encode($datos['Modulo']);?></b></td>
                                                    <td style="font-size:40px;color:<?php echo $color ?>"><b><?php echo $datos['Cantidad']; ?></b></td>
                                                    <td nowrap class="alert alert-<?php echo $estadoColOps?>" style="font-size:30px;"><b>
                                                        <?php
                                                        $arrIssues = explode(',',$datos['Issues']);
                                                        foreach ($arrIssues as $key => $nro_issue) {
                                                        ?>
                                                        <span class="badge badge-pill badge-<?php echo $estadoColOps?>" style='font-size: 30px;' ><?php echo $nro_issue; ?></span>
                                                        <?php
                                                            }
                                                        ?>
                                                        </td>
                                                </tr>
                                            </tbody>

                                            <?php } ?>

                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->

                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>


</div>

<div class="carousel-item">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-bar-chart-o fa-fw"></i> Issues de Error Creados en el mes de <?php echo utf8_encode(strftime("%B"));?> VS <?php echo utf8_encode(strftime("%B"));?> Año Anterior</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-16">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <?php
                                            $x = 1;
                                            foreach ($issuesXCreadosVSAnoAnterior["IssuesCreadosAnoAnterior"] as $index => $datos){
                                            if($RelacionAnoAnteriortidad <= 50)
                                                {$estadoColOps = 'success';$color = "#3c763d";}
                                            if($RelacionAnoAnteriortidad > 50 && $RelacionAnoAnteriortidad <= 75)
                                                {$estadoColOps = 'warning';$color="#8a6d3b";}
                                            if($datos['cantidad'] >75)
                                                {$estadoColOps = 'danger';$color="#a94442";}
                                            ?>
                                            <tbody>
                                                <tr class="table-<?php echo $estadoColOps?>">
                                                    <td style="font-size:40px;color:<?php echo $color ?>"><b><?php echo utf8_encode($datos['Nombre_Proyecto']);?></b></td>
                                                    <td class="alert alert-<?php echo $estadoColOps?>" style="font-size:33px;"><b><span class="badge badge-pill badge-<?php echo $estadoColOps?>" style='font-size: 45px;' ><?php echo $datos['actualVSanterior']; ?></span></td>
                                                </tr>
                                            </tbody>

                                            <?php } ?>

                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->

                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>

</div>


</div>
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

</div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-6">
                    <div class="card card-default">
                        <div class="card-heading">
                            <h3><i class="fa fa-bell fa-fw"></i> Issues en Implementaci&oacute;n</h3>
                        </panel>
                        <!-- /.div-heading -->
                        <div class="card-body">
                            <div class="list-group">

                            <?php
                            	$suma = 0;
                            	//$color =array("1"=>"green", "2"=>"orange");
                            	$x = 1;
                                $sumaIssuesHaciendo = 0;
                            	foreach ($issuesHaciendo["Haciendo"] as $index => $data)
                                {
                                    $arrIssue = explode(',', $data['issues']);
                                    $cantidadIssues = count($arrIssue);
                                    $sumaIssuesHaciendo += $cantidadIssues;

                                    if($cantidadIssues < 2)                         {$colorAlertaAgente='success';}
                                    if($cantidadIssues == 2)    {$colorAlertaAgente='warning';}
                                    if($cantidadIssues >= 3 )    {$colorAlertaAgente='danger';}

                                    $arrIngOps = explode(' ',$data["ASIGNADO_A"]);

    							?>
                                 <!--<a href="#" class="list-group-item">-->
                                    <div class="alert alert-<?php echo $colorAlertaAgente?>"><?php echo $icon; ?> <?php echo html_entity_decode("<b style='font-size: 35px;".$colr."'>".utf8_encode($arrIngOps[0]). "</b> <b>Asignados: X". $cantidadAsignados." Trabajando ".$cantidadIssues." </b> ")?>
                                    <?php
                                    if(is_array($arrIssue))
                                    {
                                        foreach ($arrIssue as $key => $nroIssue) {
                                    ?>
                                    <span class="badge badge-pill badge-default" style='font-size: 30px;' > <?php echo $nroIssue; ?> </span>
                                    <?php
                                        }
                                    }

                                    ?>
                                    </div>
                                <!--</a>-->

                           <?php  }
                            $promMesa = $sumaMesa / ($x-1);
                           	$ans = ($llamadas["COUNT_LLAMADAS"]["ANSWERED"] > 0)?$llamadas["COUNT_LLAMADAS"]["ANSWERED"]:0;
                           	$menos_seg = $ans - $suma;
                            if($promMesa < 3.5)                         {$colorAlerta='success';}
                            if($promMesa >= 3.5 && $promMesa <= 4.5)    {$colorAlerta='success';}
                            if($promMesa >= 4.5 && $promMesa <= 5.000)    {$colorAlerta='warning';}
                            if($promMesa > 5.000)                         {$colorAlerta='danger';}
                           	?>
                           		<a href="#" class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Total de issues en Implementaci&oacute;n
                                </a>

                                    <div class="alert alert-<?php echo $colorAlerta?>" align="right"><div style='font-size: large;'><i class="fa fa-comment fa-fw"></i><b>ISSUES EN IMPLEMENTACI&Oacute;N: <?php echo $sumaIssuesHaciendo ?></b></div></div>



                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script type="text/javascript">
    $(function() {
    var bar = Morris.Line({
            element: 'morris-area-chart-usuarios',
            data: [<?php echo $usuariosConectados["ARRVALUES2"]?>],
            xkey: 'hora',
            ykeys: ['napsis', 'mateonet', 'apoderados'],
            labels: ['Napsis', 'MatenoNet', 'Apoderados'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true
        });



//$('#morris-area-chart-usuarios').removeClass("active");
$('#morris-area-chart-usuarios').resize(function () { bar.redraw(); });
setInterval(function(){window.dispatchEvent(new Event('resize'));},3*10000);
//your code to be executed after 1 seconds



    });


    </script>
    <script language="JavaScript" type="text/javascript">
    function show5(){
        if (!document.layers&&!document.all&&!document.getElementById)
        return

         var Digital=new Date()
         var hours=Digital.getHours()
         var minutes=Digital.getMinutes()
         var seconds=Digital.getSeconds()

        var dn="PM"
        if (hours<12)
        dn="AM"
        if (hours>12)
        hours=hours-12
        if (hours==0)
        hours=12

         if (minutes<=9)
         minutes="0"+minutes
         if (seconds<=9)
         seconds="0"+seconds
        //change font size here to your desire
        myclock="<font size='5' face='Arial' >"+hours+":"+minutes+":"
         +seconds+" "+dn+"</font>"
        if (document.layers){
        document.layers.liveclock.document.write(myclock)
        document.layers.liveclock.document.close()
        }
        else if (document.all)
        liveclock.innerHTML=myclock
        else if (document.getElementById)
        document.getElementById("liveclock").innerHTML=myclock
        setTimeout("show5()",1000)
         }


        window.onload=show5
         //-->
     </script>
<script src="../js/morris-data.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
